/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary : {
          300: '#72C6EF',
          500: '#4888BC',
          700: '#0C5A99',
          800: '#004E8F',
        }
      },
    },
    fontFamily: {
      sans: 'Inter, sans-serif',
    },
  },
  plugins: [],
}

