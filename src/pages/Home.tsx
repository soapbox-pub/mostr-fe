import React, { useState } from 'react';

import { Button } from '../components/Button';
import { Input } from '../components/Input';
import { Person } from '../components/Person';
import { Relay } from '../components/Relay';
import { Users } from '../components/Users';
import { useNodeinfo } from '../hooks/useNodeInfo';
import Layout from '../Layout';
import { lookup, redirectNostr } from '../utils/lookup';

function Home() {
  const [acct, setAcct] = useState('');
  const [platform, setPlatform] = useState('fediverse'); // Default platform is fediverse
  const [error, setError] = useState<Error | undefined>();

  const { data: nodeinfo } = useNodeinfo();

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();

    try {
      const { nprofile } = await lookup(acct, platform);
      redirectNostr(nprofile);
    } catch(e) {
      setError(e as Error);
    }
  };

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    setAcct(e.target.value);
    setError(undefined);
  };

  const handlePlatformChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setPlatform(e.target.value);
  };

  // Define patterns based on platform
  const patterns: Record<string, string> = {
    fediverse: '^@?.+@[^@]+',
    threads: '^@?\\w+',
    bluesky: '^[\\w.]+'
  };

  // Define placeholder based on platform
  const placeholder: Record<string, string> = {
    fediverse: 'ex. mk@spinster.xyz',
    threads: 'ex. @potus',
    bluesky: 'ex. jay.bsky.team'
  };

  return (
    <Layout ellipse='top'>
      <div className='py-16 space-y-12'>

        <div className='space-y-8'>
          <div className='text-center space-y-2'>
            <h1 className='text-5xl font-bold leading-tight'>Mostr</h1>
            <h2 className='leading-tight'>Follow cross-platform accounts on Nostr</h2>
          </div>

          <form onSubmit={handleSubmit}>
            <Input
              value={acct}
              placeholder={placeholder[platform]}
              pattern={patterns[platform]} // Set pattern based on selected platform
              onChange={handleChange}
              append={
                <>
                  <select value={platform} onChange={handlePlatformChange} className='rounded-xl mr-2 bg-white/10 text-white/70 px-2 focus:outline-none'>
                    <option className='text-gray-800' value='fediverse'>Fediverse</option>
                    <option className='text-gray-800' value='threads'>Threads</option>
                    <option className='text-gray-800' value='bluesky'>Bluesky</option>
                  </select>
                  <Button type='submit'>Search</Button>
                </>
              }
            />
            
            {!error && (
              <p className='mt-2 text-center text-sm opacity-50'>
                Change the dropdown to look up users on other platforms
              </p>
            )}
            
            {error && (
              <p className='bg-red-700 rounded-md inline-block px-1 text-sm ml-2'>
                {error.message}
              </p>
            )}
          </form>
     
        </div>

        <div className='space-y-12'>
          <div className='space-y-4'>
            <h2 className='text-2xl font-bold text-center'>
              Users Across the Bridge
            </h2>
            <Users />
          </div>

          <div className='space-y-4'>
            <h2 className='text-2xl font-bold text-center'>
              Fediverse
            </h2>

            <div className='grid gap-4 grid-cols-3 grid-rows-2'>
              <Person name='Taylor Lorenz' platform='fediverse' acct='taylorlorenz@mastodon.social' avatar='https://files.mastodon.social/accounts/avatars/000/002/400/original/ce3d1f56519ccf33.png' />
              <Person name='Greta Thunberg' platform='fediverse' acct='gretathunberg@mastodon.nu' avatar='https://media.mastodon.nu/accounts/avatars/109/303/381/328/440/258/original/cae3e3f7a0006811.png' />
              <Person name='George Takei' platform='fediverse' acct='georgetakei@universeodon.com' avatar='https://media.universeodon.com/accounts/avatars/109/349/320/508/690/443/original/45dcbcd5cc272b39.jpg' />
              <Person name='Nina Paley' platform='fediverse' acct='ninapaley@spinster.xyz' avatar='https://media.spinster.xyz/a758411901e41da8dbcccd6699d31cc4462bc6f83509e375fe4d266fdc07f883.jpg' />
              <Person name='Richard Stallman' platform='fediverse' acct='rms@mastodon.xyz' avatar='https://6-28.mastodon.xyz/accounts/avatars/110/426/658/158/664/185/original/f85652a785470404.png' />
              <Person name='Bill Ottman' platform='fediverse' acct='ottman@minds.com' avatar='https://www.minds.com/icon/100000000000000341/large/1349106435/1689009603/1660028401' />
              <Person name='Mastodon' platform='fediverse' acct='Mastodon@mastodon.social' avatar='https://files.mastodon.social/accounts/avatars/000/013/179/original/b4ceb19c9c54ec7e.png' />
              <Person name='Ryan Barrett' platform='fediverse' acct='snarfed.org@snarfed.org' avatar='https://proxy.spinster.xyz/proxy/yUkcVynwfRobioPQehL21tjZZt3AnPRkD-cFNmgmXl0/aHR0cHM6Ly9zbmFyZmVkLm9yZy9yeWFuX3Byb2ZpbGVfbWVkaXVtLmpwZw/ryan_profile_medium.jpg' />
              <Person name='Alex Gleason' platform='fediverse' acct='alex@gleasonator.com' avatar='https://media.gleasonator.com/aae0071188681629f200ab41502e03b9861d2754a44c008d3869c8a08b08d1f1.png' />
            </div>
            </div>

            <div className=''>
              <h2 className='text-2xl font-bold text-center'>
                Bluesky
              </h2>
              <p className='text-center text-sm opacity-50 mb-6'>
                Bluesky accounts must opt-in to use Bridgy Fed
              </p>

              <div className='grid gap-4 grid-cols-3 grid-rows-1'>
                <Person name='Jay Graber' platform='bluesky' acct='jay.bsky.team' avatar='https://cdn.bsky.app/img/avatar/plain/did:plc:oky5czdrnfjpqslsw2a5iclo/bafkreihidru2xruxdxlvvcixc7lbgoudzicjbrdgacdhdhxyfw4yut4nfq@jpeg' />
                <Person name='Keely VerteDinde' platform='bluesky' acct='vertedin.de' avatar='https://cdn.bsky.app/img/avatar/plain/did:plc:2xsskxjexbop4q44kh5lw366/bafkreihhbkwmjr2rttvazdhzsahdcxieqssag353yy6drv74n7yj2yjyce@jpeg' />
                <Person name='Peter De Toit' platform='bluesky' acct='peterdutoit.com' avatar='https://cdn.bsky.app/img/avatar/plain/did:plc:6sfrfhw2vlakpteolhj75stl/bafkreihlfjeyf2jpzd4isv5xf62l2u4vklns4weaswowbauuwdm2qrbkk4@jpeg' />
              </div>
            </div>

            <div className=''>
              <h2 className='text-2xl font-bold text-center'>
                Threads
              </h2>
              <p className='text-center text-sm opacity-50 mb-6'>
                Threads accounts must opt-in to Federation
              </p>

              <div className='grid gap-4 grid-cols-3 grid-rows-1'>
                <Person name='President Joe Biden' platform='threads' acct='@potus' avatar='/assets/potus.jpg' />
                <Person name='John Green' platform='threads' acct='@johngreenwritesbooks' avatar='/assets/johngreenwritesbooks.jpg' />
                <Person name='Victoria Song' platform='threads' acct='@vicmsong' avatar='/assets/vicmsong.jpg' />
                <Person name='PJ Vogt' platform='threads' acct='@pjvogt' avatar='/assets/pjvogt.jpg' />
                <Person name='Nilay Patel' platform='threads' acct='@reckless1280' avatar='/assets/reckless1280.jpg' />
                <Person name='Chris Welch' platform='threads' acct='@chriswelch' avatar='/assets/chriswelch.jpg' />
              </div>
            </div>
        </div>

        {nodeinfo && (
          <div className='space-y-6'>
            <h2 className='text-2xl font-bold text-center'>
              Add relays to discover people and&nbsp;content
            </h2>

            <div className='space-y-2'>
              <Relay url={nodeinfo.metadata.relay} />
            </div>
          </div>
        )}

        <div className='space-y-6'>
          <h2 className='text-2xl font-bold'>
            Learn More 
          </h2>
          <ul className='list-disc underline pl-8'>
            <li><a href='https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/' target='_blank'>About the Mostr Bridge</a></li>
            <li><a href='https://soapbox.pub/blog/follow-bluesky/' target='_blank'>Following Bluesky Accounts fron Nostr</a></li>
            <li><a href='https://soapbox.pub/blog/mostr-zaps/' target='_blank'>Zapping Across the Bridge</a></li>          
          </ul>

        </div>

        <footer className='text-white divide-x-dot flex flex-wrap items-center justify-center'>
          <a href='https://uptime.mostr.pub/' target='_blank'>Status</a>
          <a href='https://gitlab.com/soapbox-pub/mostr' target='_blank'>Source code</a>
          <a href='https://njump.me/npub1d03clrrrma7mlpxm0mz2dehmhkx3nh9rhxqwltgctpwyduztymustepzfs' target='_blank'>Follow Mostr</a>
          <a href='https://soapbox.pub/donate' target='_blank'>Support us!</a>
        </footer>

        <a href='https://soapbox.pub' target='_blank' className='block'>
          <img src='/assets/soapbox.svg' alt='Soapbox' className='mx-auto h-9 brightness-0 invert' />
        </a>
      </div>
    </Layout>
  );
}

export default Home;
