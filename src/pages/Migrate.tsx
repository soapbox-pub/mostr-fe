/* eslint-disable react-hooks/exhaustive-deps */
import { NostrEvent, NPool, NRelay1 } from '@soapbox/nspec';
import { nip19 } from 'nostr-tools';
import { useEffect, useMemo, useRef, useState } from 'react';

import { Button } from '../components/Button';
import { Input } from '../components/Input';
import { ProfileCard } from '../components/ProfileCard';
import Spinner from '../components/Spinner';
import TimeAgo from '../components/TimeAgo';
import { useMetadata } from '../hooks/useMetadata';
import { useNodeinfo } from '../hooks/useNodeInfo';
import Layout from '../Layout';

function Migrate() {
  const [pubkey, setPubkey] = useState<string>();
  const [profile, setProfile] = useState<NostrEvent>();
  const [contacts, setContacts] = useState<NostrEvent>();
  const [lastFollowed, setLastFollowed] = useState<NostrEvent>();
  const [acct, setAcct] = useState<string>('');
  // const [actor, setActor] = useState<unknown>();
  const [actor] = useState<unknown>();

  const [fetching, setFetching] = useState<boolean>(true);
  const [confirmed, setConfirmed] = useState<boolean>(false);

  const { data: nodeinfo } = useNodeinfo();
  const relayUrls = useRef<string[]>([]);

  const lastFollowedPukey = contacts?.tags.reverse().find(([name]) => name === 'p')?.[1];
  const lastFollowedNpub = lastFollowedPukey ? nip19.npubEncode(lastFollowedPukey) : undefined;

  const lastFollowedMetadata = useMetadata(lastFollowed);
  const metadata = useMetadata(profile);

  const handleLogin = async () => {
    try {
      setPubkey(await window.nostr?.getPublicKey());
    } catch (e) {
      console.debug(e);
    }
  };

  const renderLogin = () => {
    return (
      <div className='text-center space-y-8'>
        <div className='space-y-2'>
          <h1 className='text-2xl font-bold leading-tight'>
            ActivityPub Follows Migration Tool
          </h1>
          <h2 className='leading-tight text-lg'>
            Locate your ActivityPub follows and follow them on Nostr
          </h2>
        </div>

        {window.nostr ? (
          <Button onClick={handleLogin}>Nostr extension login</Button>
        ) : (
          <div>Please install a <a className='underline' href='https://nostr.net/#nip-07-browser-extensions' target='_blank'>Nostr browser extension</a> to continue.</div>
        )}
      </div>
    )
  }

  const renderNostrConfirm = () => {
    if (fetching && (!profile || !contacts)) {
      return <Spinner />;
    }
    return (
      <div className='text-center space-y-8'>
        <h1 className='text-2xl font-bold leading-tight'>
          Confirm your Nostr account
        </h1>
        <h2 className='leading-tight text-lg'>
          We found a Nostr account with the following details. Is this you?
        </h2>

        <ProfileCard name={metadata.name} avatar={metadata.picture} />

        {contacts ? (
            <h2 className='leading-tight text-lg'>
            You follow {contacts.tags.filter(([name]) => name === 'p').length ?? 0} accounts.<br />

            {(lastFollowedNpub && lastFollowedMetadata) && (
              <>
                You last followed <a className='underline' href={`https://njump.me/${lastFollowedNpub}`} target='_blank'>
                  {lastFollowedMetadata.name ?? lastFollowedNpub?.substring(0, 8)}
                </a> <TimeAgo pastDate={new Date(contacts.created_at * 1000)} />.
              </>
            )}
          </h2>
        ) : (
          <h2 className='leading-tight text-lg'>
            You don't follow any accounts.
          </h2>
        )}

        <Button onClick={() => setConfirmed(true)}>
          Yes, migrate my follows
        </Button>
      </div>
    )
  }

  const handleActivityPubSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!actor) return;

    // const signal = AbortSignal.timeout(1000);
    setFetching(true);

    try {
      // const { inbox } = actor as { inbox: string };
      // const { id } = await fetch(inbox, { signal }).then((res) => res.json());
      // setLastFollowedPukey(id);
    } catch (e) {
      console.error(e);
    } finally {
      setFetching(false);
    }
  }

  const renderActivityPub = () => {
    return (
      <div className='text-center space-y-8'>
        <h1 className='text-2xl font-bold leading-tight'>
          Enter your ActivityPub address
        </h1>
        <form onSubmit={handleActivityPubSubmit}>
          <Input
            value={acct}
            onChange={(e) => setAcct(e.target.value)}
            placeholder='e.g. alex@gleasonator.com'
            pattern='^@?.+@[^@]+'
            append={<Button type='submit'>Search</Button>}
          />
        </form>
      </div>
    );
  }

  const renderMigration = () => {
    return (
      <div className='text-center space-y-8'>
        <h1 className='text-2xl font-bold leading-tight'>
          Locating and following in progress…
        </h1>
      </div>
    );
  }

  const renderStep = () => {
    if (!pubkey) {
      return renderLogin();
    }
    if (!confirmed) {
      return renderNostrConfirm();
    }
    if (!actor) {
      return renderActivityPub();
    }
    return renderMigration();
  }

  const pool = useMemo(() => {
    return new NPool({
      open(url) {
        return new NRelay1(url);
      },
      async reqRelays() {
        return relayUrls.current;
      },
      async eventRelays() {
        return relayUrls.current;
      }
    });
  }, []);

  useEffect(() => {
    const relay = nodeinfo?.metadata?.relay;
    if (relay) {
      relayUrls.current = [relay];
    }
  }, [nodeinfo?.metadata]);

  useEffect(() => {
    if (!lastFollowedPukey) return;
    const signal = AbortSignal.timeout(5000);

    pool
      .query([{ kinds: [0], authors: [lastFollowedPukey], limit: 1 }], { signal })
      .then(([event]) => {
        if (event && (!lastFollowed || (event.created_at > lastFollowed.created_at))) {
          setLastFollowed(event);
        }
      })
      .catch(console.error);
  }, [lastFollowedPukey]);

  useEffect(() => {
    if (!pubkey) return;
    const signal = AbortSignal.timeout(1000);
    setFetching(true);

    Promise.all([
      pool
        .query([{ kinds: [0], authors: [pubkey], limit: 1 }], { signal })
        .then(([event]) => {
          if (event && (!profile || (event.created_at > profile.created_at))) {
            setProfile(event);
          }
        }),
      pool
        .query([{ kinds: [3], authors: [pubkey], limit: 1 }], { signal })
        .then(([event]) => {
          if (event && (!contacts || (event.created_at > contacts.created_at))) {
            setContacts(event);
          }
        }),
    ])
      .catch(console.error)
      .finally(() => setFetching(false));
    }, [pubkey]);

  return (
    <Layout>
      {renderStep()}
    </Layout>
  );
}

export default Migrate;
