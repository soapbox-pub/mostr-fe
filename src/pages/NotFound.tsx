import { Link } from 'react-router-dom';

import Layout from '../Layout';

function NotFound() {
  return (
    <Layout>
      <div className='text-center space-y-2'>
        <h1 className='text-2xl font-bold leading-tight'>Page Not Found</h1>
        <h2 className='leading-tight text-lg'>
          <Link to='/'>Go back home</Link>
        </h2>
      </div>
    </Layout>
  );
}

export default NotFound;
