import { Ellipse } from './components/Ellipse';

interface ILayout {
  ellipse?: 'top' | 'center' | false;
  children: React.ReactNode;
}

const Layout: React.FC<ILayout> = ({ children, ellipse = 'center' }) => {
  return (
    <>
      {ellipse && (
        <Ellipse className={ellipse === 'top' ? '-top-20' : undefined} />
      )}

      <div className='flex min-h-screen px-2 items-center justify-center max-w-lg mx-auto '>
        {children}
      </div>
    </>
  );
}

export default Layout;
