import clsx from 'clsx';

import ellipse from '../assets/ellipse.svg';

interface IEllipse {
  className?: string;
}

/** Background graphic. */
export const Ellipse: React.FC<IEllipse> = ({ className }) => {
  return (
    <img
      className={clsx('absolute inset-x-0 -z-10 w-screen h-screen overflow-hidden object-none', className)}
      src={ellipse}
    />
  );
};