export interface AvatarProps {
  src?: string;
  alt?: string;
  size: number;
}

export const Avatar: React.FC<AvatarProps> = ({ src, alt, size }) => {
  return (
    <img
      className='rounded-full bg-white/10 backdrop-blur over object-cover'
      style={{ width: size, height: size }}
      src={src}
      alt={alt}
    />
  );
};