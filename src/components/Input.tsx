export interface InputProps {
  value: string;
  placeholder?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  pattern?: string;
  readOnly?: boolean;
  append?: React.ReactNode;
}

export const Input: React.FC<InputProps> = ({ value, placeholder, onChange, pattern, readOnly, append }) => {
  return (
    <div className='bg-white/10 flex justify-between py-1.5 px-3 rounded-2xl border border-white/10 focus-within:border-white/50 backdrop-blur'>
      <input
        type='text'
        className='bg-transparent text-white placeholder-white/50 w-full focus:outline-none px-2'
        placeholder={placeholder}
        pattern={pattern}
        value={value}
        onChange={onChange}
        readOnly={readOnly}
      />
      {append}
    </div>
  );
};