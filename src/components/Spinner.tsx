import './Spinner.css';

interface SpinnerProps {
  /** Width and height of the spinner in pixels. */
  size?: number;
  /** Whether to display "Loading..." beneath the spinner. */
  withText?: boolean;
}

/** Spinning loading placeholder. */
const Spinner: React.FC<SpinnerProps> = ({ size = 30, withText = true }) => (
  <div className='flex flex-col space-y-2 items-center justify-center'>
    <div className='spinner' style={{ width: size, height: size }}>
      {Array.from(Array(12).keys()).map(i => (
        <div key={i}>&nbsp;</div>
      ))}
    </div>

    {withText && (
      <span className='text-center text-sm'>Loading…</span>
    )}
  </div>
);

export default Spinner;
