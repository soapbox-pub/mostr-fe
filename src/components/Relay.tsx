import { IconCopy } from '@tabler/icons-react';

import { Input } from './Input';
import { Tooltip } from './Tooltip';

export interface RelayProps {
  url: string;
}

export const Relay: React.FC<RelayProps> = ({ url }) => {
  const copyToClipboard = () => {
    navigator.clipboard.writeText(url);
  };

  return (
    <Input
      value={url}
      append={(
        <Tooltip text='Copied!'>
          <div>
            <button className='bg-white/10 hover:bg-white/20 p-3 rounded-xl transition' onClick={copyToClipboard}>
              <IconCopy size={16} />
            </button>
          </div>
        </Tooltip>
      )}
      readOnly
    />
  );
};