import { Avatar } from './Avatar';
import { Card } from './Card';

export interface ProfileCardProps {
  name?: string;
  avatar?: string;
  href?: string;
  target?: React.HTMLAttributeAnchorTarget;
}

export const ProfileCard: React.FC<ProfileCardProps> = ({ name, avatar, href, target }) => {
  return (
    <Card href={href} target={target}>
      <Avatar src={avatar} size={64} />
      <h1 className='text-center text-sm'>{name}</h1>
    </Card>
  );
};