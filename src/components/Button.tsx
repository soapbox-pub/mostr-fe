export interface ButtonProps {
  children: React.ReactNode;
  type?: 'button' | 'submit';
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  disabled?: boolean;
}

export const Button: React.FC<ButtonProps> = ({ children, type = 'button', onClick, disabled }) => {
  return (
    <button
      className='bg-white rounded-xl py-3 px-4 text-primary-700 hover:text-primary-500 font-semibold text-sm leading-tight transition'
      disabled={disabled}
      onClick={onClick}
      type={type}
    >
      {children}
    </button>
  );
};