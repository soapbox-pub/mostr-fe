export interface CardProps {
  href?: string;
  target?: React.HTMLAttributeAnchorTarget;
  children?: React.ReactNode;
}

export const Card: React.FC<CardProps> = ({ href, target, children }) => {
  const Elem = href ? 'a' : 'div';
  return (
    <Elem
      className='bg-white/10 hover:bg-white/20 flex flex-col justify-center items-center p-4 gap-2 rounded-2xl border border-white/0 focus-within:border-white/50 backdrop-blur transition'
      href={href}
      target={target}
    >
      {children}
    </Elem>
  );
};