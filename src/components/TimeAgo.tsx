import React, { useEffect, useState } from 'react';

interface TimeAgoProps {
  pastDate: Date;
}

const TimeAgo: React.FC<TimeAgoProps> = ({ pastDate }) => {
  const [timeAgo, setTimeAgo] = useState('');

  const calculateTimeAgo = (date: Date) => {
    const seconds = Math.floor((Date.now() - date.getTime()) / 1000);
    let interval = seconds / 31536000;

    if (interval > 1) {
      return Math.floor(interval) + ' years ago';
    }
    interval = seconds / 2592000;
    if (interval > 1) {
      return Math.floor(interval) + ' months ago';
    }
    interval = seconds / 86400;
    if (interval > 1) {
      return Math.floor(interval) + ' days ago';
    }
    interval = seconds / 3600;
    if (interval > 1) {
      return Math.floor(interval) + ' hours ago';
    }
    interval = seconds / 60;
    if (interval > 1) {
      return Math.floor(interval) + ' minutes ago';
    }
    return Math.floor(seconds) + ' seconds ago';
  };

  useEffect(() => {
    const update = () => {
      setTimeAgo(calculateTimeAgo(pastDate));
    };
    update();
    const tid = setInterval(update, 60000);
    return () => clearInterval(tid);
  }, [pastDate]);

  return <span>{timeAgo}</span>;
}

export default TimeAgo;
