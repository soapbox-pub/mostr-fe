import React from 'react';
import CountUp from 'react-countup';
import { GiOstrich } from 'react-icons/gi';
import { PiFediverseLogo } from 'react-icons/pi';

import { useNodeinfo } from '../hooks/useNodeInfo';

export const Users: React.FC = () => {
  const { data: nodeinfo, error } = useNodeinfo();

  const nostrUsersCount = nodeinfo?.metadata.stats.nostr.users ?? 0;
  const activityPubUsersCount = nodeinfo?.metadata.stats.activitypub.users ?? 0;

  return (
    <div className="text-center">
      {error ? (
        <p className="text-red-500">Error: {String(error)}</p>
      ) : (
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          <div className="flex flex-col items-center p-4 shadow-lg bg-white/10 rounded-2xl border border-white/0 backdrop-blur transition">
            <GiOstrich className="text-4xl mb-2" />
            <CountUp className='font-bold' end={nostrUsersCount} duration={2} separator="," />
            <p className="text-lg">Nostr Users</p>
          </div>
          <div className="flex flex-col items-center p-4 shadow-lg bg-white/10 rounded-2xl border border-white/0 backdrop-blur transition">
            <PiFediverseLogo className="text-4xl mb-2" />
            <CountUp className='font-bold' end={activityPubUsersCount} duration={2} separator="," />
            <p className="text-lg">Fediverse Users</p>
          </div>
        </div>
      )}
    </div>
  );
};
