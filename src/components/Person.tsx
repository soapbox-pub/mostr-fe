import { useEffect, useState } from 'react';

import { LookupResult, lookup } from '../utils/lookup';

import { ProfileCard } from './ProfileCard';

export interface PersonProps {
  name: string;
  avatar: string;
  acct: string;
  platform: string; 
}

export const Person: React.FC<PersonProps> = ({ name, avatar, acct, platform }) => {
  const [result, setResult] = useState<LookupResult>();

  useEffect(() => {
    lookup(acct, platform).then(setResult); // Pass platform to lookup function
  }, [acct, platform]);

  return (
    <ProfileCard
      name={name}
      avatar={avatar}
      href={result ? `https://njump.me/${result.nprofile}` : '#'}
      target='_blank'
    />
  );
};