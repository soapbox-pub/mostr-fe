import { nip19 } from 'nostr-tools';

export interface LookupResult {
  pubkey: string;
  relays: string[];
  nprofile: `nprofile1${string}`;
}

/** Look up ActivityPub account from Nostr. */
export async function lookup(acct: string, platform: string): Promise<LookupResult> {
  
  let base: string;
  switch (platform) {
    case 'fediverse':
      base = '';
      break;
    case 'threads':
      base = '_at_threads.net';
      break;
    case 'bluesky':
      base = '_at_bsky.brid.gy';
      break;
    default:
      throw new Error('Invalid platform');
  }
  
  let name: string;
  switch (platform) {
    case 'fediverse':
      name = acct.replace(/^@/, '').replace('@', '_at_');
      break;
    case 'threads':
      name = acct.replace(/^@/, '');
      break;
    case 'bluesky':
      // For Bluesky, the account name is already in the correct format
      name = acct;
      break;
    default:
      throw new Error('Invalid account: ' + acct);
  }  
  
  const response = await fetch(`https://mostr.pub/.well-known/nostr.json?name=${name + base}`);

  const platformErrors: Record<string, string> = {
    fediverse: 'Account not found',
    threads: 'Account not found: This user may not opt-in to Federation',
    bluesky: 'Account not found: This user may not opt-in to Bridgy Fed'
  };


  if (!response.ok) {
    throw new Error(platformErrors[platform]);
  }

  const json = await response.json() as { names: Record<string, string>, relays: Record<string, string[]> };

  const pubkey = json.names[name + base];
  const relays = json.relays[pubkey];
  const nprofile = nip19.nprofileEncode({ pubkey, relays });

  return { pubkey, relays, nprofile };
}

export function redirectNostr(nprofile: `nprofile1${string}`) {
  location.href = `https://njump.me/${nprofile}`;
}