import { NostrEvent, NostrMetadata, NSchema as n } from '@soapbox/nspec';
import { useMemo } from 'react';

export function useMetadata(event: NostrEvent | undefined): NostrMetadata {
  return useMemo(() => {
    try {
      return n.json().pipe(n.metadata()).parse(event?.content);
    } catch (e) {
      return {};
    }
  }, [event]);
}