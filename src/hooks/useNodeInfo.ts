import { UseQueryResult, useQuery } from 'react-query';

export interface MostrNodeinfo {
  metadata: {
    relay: string;
    stats: {
      activitypub: {
        users: number;
      }
      nostr: {
        users: number;
      }
    }
  };
}

export function useNodeinfo(): UseQueryResult<MostrNodeinfo> {
  return useQuery('nodeinfo', async () => {
    const response = await fetch('https://mostr.pub/nodeinfo/2.1');
    return response.json();  
  });
}